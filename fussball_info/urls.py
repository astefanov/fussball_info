"""
fussball_info URL Configuration
"""
from django.conf.urls import url
from fussball_info.core.views import MatchesView, TeamsView, NextMatchdayView

urlpatterns = [
    url(r'^matches/upcoming/', NextMatchdayView.as_view()),
    url(r'^matches/', MatchesView.as_view()),
    url(r'^teams/', TeamsView.as_view()),
]
