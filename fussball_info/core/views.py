from django.views.generic import TemplateView
from fussball_info.core.client import OpenLigaClient

POINTS_FOR_WIN = 3
POINTS_FOR_DRAW = 1

class MatchesView(TemplateView):
    template_name = 'core/matches.html'

    def get_context_data(self, **kwargs):
        client = OpenLigaClient()
        return {'matches': client.get_current_season_matches()}


class NextMatchdayView(TemplateView):
    template_name = 'core/matches_upcoming.html'

    def get_context_data(self, **kwargs):
        client = OpenLigaClient()
        return {'matches': client.get_matches_for_next_matchday()}


class TeamsView(TemplateView):
    template_name = 'core/teams.html'

    def get_context_data(self, **kwargs):
        client = OpenLigaClient()
        matches = client.get_current_season_matches()
        teams = {t['TeamName']: {
            'TeamName': t['TeamName'],
            'ShortName': t['ShortName'],
            'TeamIconUrl': t['TeamIconUrl'],
            'Matches': 0,
            'Wins': 0,
            'Losses': 0,
            'Draws': 0,
            'WinLossRatio': 0,
        } for t in client.get_teams()}

        for match in matches:
            # Consider only finished matches (someone may be looking at the table while some matches are being played)
            if match['MatchIsFinished']:
                team1 = match['Team1']['TeamName']
                team2 = match['Team2']['TeamName']
                result = match['MatchResults'][0] if match['MatchResults'][0]['ResultTypeID'] == 2 else match['MatchResults'][1]

                points_team_1 = result['PointsTeam1']
                points_team_2 = result['PointsTeam2']

                teams[team1]['Matches'] += 1
                teams[team2]['Matches'] += 1

                if points_team_1 > points_team_2:
                    teams[team1]['Wins'] += 1
                    teams[team2]['Losses'] += 1
                elif points_team_1 < points_team_2:
                    teams[team1]['Losses'] += 1
                    teams[team2]['Wins'] += 1
                else:
                    teams[team1]['Draws'] += 1
                    teams[team2]['Draws'] += 1

        for team_name, team_info in teams.iteritems():
            wins = float(team_info['Wins'])
            losses = float(team_info['Losses'])
            # 1e-10 is added so we don't divide by zero
            teams[team_name]['WinLossRatio'] = round(wins / (wins + losses) * 100, 2)
            teams[team_name]['Score'] = team_info['Wins'] * POINTS_FOR_WIN + team_info['Draws'] * POINTS_FOR_DRAW

        return {'teams': sorted(teams.values(), key=lambda x: x['Score'], reverse=True)}
