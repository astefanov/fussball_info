import datetime
from urlparse import urljoin
import requests


class APIClient(object):
    BASE_URL = ''

    def _build_endpoint(self, path, **params):
        raise NotImplemented

    def default_headers(self):
        raise NotImplemented


class OpenLigaClient(APIClient):
    # Bundesliga season ends in May and starts in August.
    # The boundary is the half-time between season's start and season's end, i.e, end of June and beginning of July.
    # So the 'current season' in June will still be the previous season that just finished.
    # The 'current season' in July will however be the next season, that is about to start.
    SEASONS_BOUNDARY_MONTH = 7
    BASE_URL = 'https://www.openligadb.de/api/'
    DEFAULT_BUNDES_LIGA = 'bl1'
    ACCEPT_CONTENT_FORMAT = 'application/json'

    def _request(self, endpoint):
        return requests.get(endpoint, headers={'Accept': self.ACCEPT_CONTENT_FORMAT})

    def _build_endpoint(self, *url_parts):
        endpoint = urljoin(self.BASE_URL, '/'.join(map(str, url_parts)))
        print(endpoint)
        return endpoint

    def _get_current_season_start_year(self):
        now = datetime.datetime.now()
        year, month = now.year, now.month
        adjustment = 1 if month < 7 else 0
        return str(year - adjustment)

    def get_matches(self, season_start_year, matchday=''):
        endpoint = self._build_endpoint('getmatchdata', self.DEFAULT_BUNDES_LIGA, season_start_year, matchday)
        return self._request(endpoint).json()

    def get_current_season_matches(self):
        season_start_year = self._get_current_season_start_year()
        return self.get_matches(season_start_year)

    def get_current_matchday(self):
        endpoint = self._build_endpoint('getcurrentgroup', self.DEFAULT_BUNDES_LIGA)
        return self._request(endpoint).json()['GroupOrderID']

    def get_matches_for_matchday(self, matchday):
        season_start_year = self._get_current_season_start_year()
        return self.get_matches(season_start_year, matchday)

    def get_matches_for_current_matchday(self):
        matchday = self.get_current_matchday()
        return self.get_matches_for_matchday(matchday)

    def get_matches_for_next_matchday(self):
        current_matchday = self.get_current_matchday()
        current_matches = self.get_matches_for_current_matchday()
        current_matches = sorted(current_matches, key=lambda x: datetime.datetime.strptime(x['MatchDateTime'], '%Y-%m-%dT%H:%M:%S'))
        first_date = datetime.datetime.strptime(current_matches[0]['MatchDateTime'], '%Y-%m-%dT%H:%M:%S')
        if first_date < datetime.datetime.now():
            # Current Matchday has already started, so show the matches for the following Matchday
            next_matchday = current_matchday + 1
        else:
            # Next Matchday is the (not yet started) 'current' matched day
            next_matchday = current_matchday
        return self.get_matches_for_matchday(next_matchday)

    def get_match_by_id(self, match_id):
        endpoint = self._build_endpoint('getmatchdata', match_id)
        return self._request(endpoint).json()

    def get_teams(self, year=''):
        year = year or self._get_current_season_start_year()
        endpoint = self._build_endpoint('getavailableteams', self.DEFAULT_BUNDES_LIGA, year)
        return self._request(endpoint).json()
